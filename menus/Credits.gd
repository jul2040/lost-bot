extends Control

func _ready():
	set_process(false)

func _process(delta):
	$Camera.position.y = min($Camera.position.y+(delta*50), $MaxCameraPos.position.y)

func _input(event):
	if(event is InputEventKey and event.pressed):
		Loader.goto_scene("res://menus/MainMenu.tscn")

func _on_StartTimer_timeout():
	set_process(true)
