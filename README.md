
# Lost Bot

A puzzle game where you control a character whose inputs loop.

Made for the Godot Wild Jam #35.

[You can play it on itch.io here](https://jul2040.itch.io/lost-bot)

The game features a level editor, and for reference, here is how the levels are saved in base 64 strings.

# Level Code Specification

The level codes are saved as follows

They are encoded as base 64

The first byte is an integer that stores the world theme that should be applied to the level

the next 128 bytes are a 8x8 grid of 2 byte tiles, starting from the top left and going right

# First Byte

The first byte of each tile contains the data for the object layer in the most significant 4 bits, and the data for the conveyor in the least significant 4 bits.

# Object bits

The object data stores the index in a list of objects

the list is as follows: [robot, world 1 box, world 2 box, world 3 box, robot with pumpkin]

# Conveyor bits

The conveyor data is laid out as follows

most significant 2 bits: stores which world the conveyor belt should be themed with.

00 = no conveyor

01 = world 1

10 = world 2

11 = world 3

the third bit stores whether the conveyor should be vertical or horizontal

0 = horizontal

1 = vertical

the fourth bit stores whether it should be flipped or not

0 = not flipped

1 = flipped

example: 1001 = world 2 left facing conveyor

# Second Byte

This byte stores the tile layer.

It stores an index in the tilemap.

# Compression

The level codes get compressed before you copy them.

The compression works by taking repeated characters in the base 64 string and encoding how many times they get repeated

it looks for characters that get repeated 4 or more times, and replaces them with the following string

([CHARACTER TO BE REPEATED][2 DIGIT HEX NUMBER]

example: GGGGGGGG would be encoded as (G08
