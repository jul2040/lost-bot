extends GridObject

export(Vector2) var direction = Vector2.UP setget set_direction
export(int) var vertical_sprite = 0
export(int) var horizontal_sprite = 1

func set_direction(d):
	direction = d
	if(direction.x == 0):
		#vertical
		sprite_ind = vertical_sprite
		flip_x = false
		flip_y = direction.y > 0
	else:
		#horizontal
		sprite_ind = horizontal_sprite
		flip_x = direction.x < 0
		flip_y = false
