extends GridObject

export(int) var down_sprite = 0
export(int) var down_sprite_frames = 1
export(int) var up_sprite = 4
export(int) var up_sprite_frames = 1
export(int) var right_sprite = 3
export(int) var right_sprite_frames = 1

func frame():
	var dir = level.next_robot_dir()
	position += dir
	flip_x = dir.x < 0
	if(dir.y > 0):
		sprite_ind = down_sprite
	elif(dir.y < 0):
		sprite_ind = up_sprite
	else:
		sprite_ind = right_sprite
