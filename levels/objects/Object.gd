extends Node
class_name GridObject

export(int) var sprite_ind:int = 0
export(int) var animation_length:int = 1
export(bool) var flip_x:bool = false
export(bool) var flip_y:bool = false
export(bool) var pushable:bool = false
export(Vector2) var position = Vector2()
var revert_pos = Vector2()
var moved:bool = false

var frame = 0
var level

func frame():
	pass
