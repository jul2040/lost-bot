extends HBoxContainer

var count = 0
var selectors = []
func show_selectors(c:int):
	cur_selector = 0
	count = c
	for s in selectors:
		s.queue_free()
	selectors = []
	for _i in range(count):
		var s = preload("res://levels/Selector.tscn").instance()
		add_child(s)
		s.connect("state_changed", self, "selector_state_changed")
		selectors.append(s)
	selectors[0].select()

var cur_selector = 0
func selector_state_changed(selector):
	if selector.state == Selector.STATES.LOCKED and cur_selector < len(selectors)-1:
		#this selector just got locked into a direction
		var index = selectors.find(selector)
		assert(index!=-1)
		cur_selector = index
		if(index >= len(selectors)-1):
			#start game
			owner.start_game()
		else:
			selectors[index+1].select()

func highlight(i:int):
	for s in selectors:
		s.lock()
	selectors[i].highlight()

const DIR_TO_VEC = [Vector2.ZERO, Vector2.UP, Vector2.DOWN, Vector2.LEFT, Vector2.RIGHT]
func get_dirs()->Array:
	var dirs = []
	for s in selectors:
		dirs.append(DIR_TO_VEC[s.dir])
	return dirs

var can_input = true
func _on_InputCooldown_timeout():
	can_input = true

func use_input():
	can_input = false
	$InputCooldown.start()
