extends AudioStreamPlayer2D

const SFX = [
	[
		preload("res://assets/sfx/world_1_walk_1.mp3"),#0
		preload("res://assets/sfx/world_1_walk_2.mp3")
	],
	[
		preload("res://assets/sfx/world_2_walk_1.mp3"),#1
		preload("res://assets/sfx/world_2_walk_2.mp3")
	],
	[
		preload("res://assets/sfx/conveyor_belt.mp3")#2
	],
	[
		preload("res://assets/sfx/wall_first_hit.mp3")#3
	],
	[
		preload("res://assets/sfx/wall_second_hit.mp3")#4
	],
	[
		preload("res://assets/sfx/box_push.mp3")#5
	],
	[
		preload("res://assets/sfx/tree.mp3")#6
	],
	[
		preload("res://assets/sfx/bush.mp3")#7
	],
	[
		preload("res://assets/sfx/stump.mp3")#8
	],
	[
		preload("res://assets/sfx/rock.wav")#9
	]
]
func play_sfx(id:int):
	var sound = SFX[id][randi()%len(SFX[id])]
	stream = sound
	play()

func _on_SFXPlayer_finished():
	queue_free()
