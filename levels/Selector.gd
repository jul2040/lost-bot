extends Control
class_name Selector

enum STATES {WAITING, SELECTED, LOCKED, LOCKED_HIGHLIGHT}
var state = STATES.WAITING
func select():
	set_state(STATES.SELECTED)

func lock():
	set_state(STATES.LOCKED)

func highlight():
	set_state(STATES.LOCKED_HIGHLIGHT)

signal state_changed(selector)
func set_state(s):
	state = s
	match state:
		STATES.LOCKED:
			$TextureRect.texture = preload("res://assets/selector/waiting.png")
		STATES.LOCKED_HIGHLIGHT:
			$TextureRect.texture = preload("res://assets/selector/playing.png")
	emit_signal("state_changed", self)

func _input(event):
	if(not event is InputEventKey):
		return
	if(not get_parent().can_input):
		return
	match state:
		STATES.SELECTED:
			if(Input.is_action_just_pressed("ui_up")):
				set_dir(DIRECTIONS.UP)
				set_state(STATES.LOCKED)
				get_parent().use_input()
			elif(Input.is_action_just_pressed("ui_down")):
				set_dir(DIRECTIONS.DOWN)
				set_state(STATES.LOCKED)
				get_parent().use_input()
			elif(Input.is_action_just_pressed("ui_left")):
				set_dir(DIRECTIONS.LEFT)
				set_state(STATES.LOCKED)
				get_parent().use_input()
			elif(Input.is_action_just_pressed("ui_right")):
				set_dir(DIRECTIONS.RIGHT)
				set_state(STATES.LOCKED)
				get_parent().use_input()

enum DIRECTIONS {NONE,UP,DOWN,LEFT,RIGHT}
var dir = DIRECTIONS.NONE
const arrows = [preload("res://assets/selector/up.png"),
		preload("res://assets/selector/down.png"),
		preload("res://assets/selector/left.png"),
		preload("res://assets/selector/right.png")
	]
func set_dir(d):
	dir = d
	$ArrowRect.texture = arrows[dir-1]
