extends Camera2D

export(float) var shake_dist:float = 3
export(float) var shake_time:float = 0.05
export(int) var max_shakes:int = 4

onready var tween = get_node("Tween")
func shake():
	shake = 0
	shaking = true
	_on_Tween_tween_all_completed()

var shake:int = 0
var shaking = false
func _on_Tween_tween_all_completed():
	if(not shaking):
		return
	var shake_pos:Vector2
	if(shake != max_shakes-1):
		shake_pos = Vector2(rand_range(-shake_dist, shake_dist),
					rand_range(-shake_dist, shake_dist))
	else:
		shake_pos = Vector2()
	if(shake < max_shakes):
		tween.interpolate_property(self, "offset",
				offset, shake_pos, shake_time,
				Tween.TRANS_ELASTIC, Tween.EASE_OUT)
		tween.start()
	shake += 1

var clicked = false
func _input(event):
	if(event is InputEventMouseButton and event.button_index == BUTTON_LEFT):
		clicked = event.pressed
		if(not clicked and get_can_move()):
			shake = max_shakes-1
			var old_shake_time = shake_time
			shake_time = 0.5
			shaking = true
			_on_Tween_tween_all_completed()
			shaking = false
			shake_time = old_shake_time
	if(not get_can_move()):
		return
	if(event is InputEventMouseMotion and clicked):
		if($Tween.is_active()):
			$Tween.stop_all()
		offset -= event.relative

var can_move = true
func set_can_move(m:bool):
	can_move = m
	if(not get_can_move()):
		shake = max_shakes-1
		var old_shake_time = shake_time
		shake_time = 0.5
		shaking = true
		_on_Tween_tween_all_completed()
		shaking = false
		shake_time = old_shake_time

func get_can_move()->bool:
	return can_move and not get_tree().paused
