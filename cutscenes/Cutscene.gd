extends Control

export(String) var next_scene:String = ""
export(int) var music = 0

func _ready():
	MusicManager.play(music)

func _input(event):
	if(event is InputEventKey and event.pressed):
		Loader.goto_scene(next_scene)
