extends Control

export(Array, Vector2) var positions:Array = []
var cur_position:int = 0

func _ready():
	_on_StillTimer_timeout()
	set_process(false)
	MusicManager.play(2)
	GameSaver.reset()

func _on_StillTimer_timeout():
	if(cur_position == len(positions)-1):
		$ContinueTimer.start()
	if(cur_position >= len(positions)):
		$StillTimer.stop()
		return
	$Camera.position = positions[cur_position]
	cur_position += 1

var waiting = false
func _on_ContinueTimer_timeout():
	$Credits/Label6.visible = true
	waiting = true

func _input(event):
	if(waiting and event is InputEventKey and event.pressed):
		Loader.goto_scene("res://menus/MainMenu.tscn")
